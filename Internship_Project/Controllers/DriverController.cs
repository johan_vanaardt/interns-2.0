﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Internship_Project.Models;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data.SqlTypes;

namespace Internship_Project.Controllers
{
    public class DriverController : ApiController
    {
        private Admin admin;
        public static Driver driver = new Driver();



        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("api/Admin/Login/{user}/{pass}")]
        public IHttpActionResult AdminLogin(string userName, string password)
        {
            admin = new Admin();
            if (!admin.CheckAdmin(userName, password))
            {
                return NotFound();
            }
            else
                return Ok();
        }



        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("api/Admin/delete/{user}")]
        public IHttpActionResult RemoveAdmin(string userName)
        {
            admin.RemoveAdmin(userName);
            return Ok();
        }



        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("api/Admin/add/{user}/{pass}")]
        public IHttpActionResult NewAdmin(string userName, string password)
        {
            admin.CreateAdmin(userName, password);
            return Ok();
        }



        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("api/Admin/Email/{user}/{oldmail}/{newmail}")]
        public IHttpActionResult ChangeEmail(string userName, string oldmail, string newmail)
        {
            if (admin.CheckEmail(userName, oldmail))
            {
                admin.ChangeEmail(userName, newmail);
                return Ok();
            }
            return BadRequest("Email does not match username!");
        }



        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("api/Admin/PassReset/{user}/{email}")]
        public IHttpActionResult ResetPass(string userName, string email)
        {
            if (admin.CheckEmail(userName, email))
            {
                //****************************************
                string randomPass = "";
                //generate random pass
                //save random pass using admin.ChangePass(userName, randomPass)
                admin.ChangePass(userName, randomPass);
                //Send Email containing randomPass and link to New Password page

                return Ok();
            }
            else
                return NotFound();
        }



        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("api/Admin/PassChange/{user}/{oldPass}/{newPass}")]
        public IHttpActionResult NewPass(string userName, string oldPass, string newPass)
        {
            if (admin.CheckAdmin(userName, oldPass))
                if (admin.ChangePass(userName, newPass))
                    return Ok();
                else return BadRequest("The password cannot be blank");
            else return NotFound();
        }


        
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("api/Admin/Logout")]
        public IHttpActionResult LogOut()
        {
            admin.Logout();
            return Ok();
        }



        ///////////////////////////////////////////////////////////////////////////////////////////////



        public IEnumerable<Driver> GetAllDrivers()//returns a list of all the drivers
        {
            int currentDriver = 0;
            List<Driver> drivers = new List<Driver>();
            while (driver.GetDriver(currentDriver) != false)
            {
                drivers.Add(driver);
                currentDriver++;
            }
            return drivers;
        }



        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("api/Driver/editduty/{id}/{dutyState}")]
        public IHttpActionResult EditDutyState(int id, int dutyState)
        {
            driver.GetDriver(id);
            driver.IsOnDuty = (dutyState == 1 ? true : false);
            driver.Active = ((dutyState == 1 || dutyState == 2) ? true : false);
            string duty = (dutyState == 1 ? "on duty" : ( dutyState == 2 ? "on standby" : "inactive"));
            return Ok($"Driver {driver.Name} has been set to {duty}");
        }



        [HttpGet]
        public IHttpActionResult GetDriver(int id)//returns a single driver with a matching ID (Search function)
        {
            /*if (id == null)
                return BadRequest();
            */
            driver.GetDriver(id);
            if (driver == null)
            {
                return NotFound();
            }
            return Ok(driver);
        }



        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("api/Driver/add/{driverName}/{dutyState}")]
        public IHttpActionResult AddDriver(string driverName, bool dutyState)//creates a new object of the 'Driver' model class
        {
            String msg;
            driver = new Driver()
            {
                Name = driverName,
                IsOnDuty = dutyState
            };
            if (dutyState)
            {
                msg = $"Driver '{driverName}' has been added as immediately on duty.";
            }
            else
            {
                msg = $"Driver '{driverName}' has been added, on standby.";
            }
            return Ok(msg);
        }



        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("api/Driver/edit/{id}/{name}/{dateJoined}")]
        public IHttpActionResult EditDriver(int id, string name, DateTime dateJoined)
        {
            driver.GetDriver(id);
            if (driver == null)
                return BadRequest("No driver matches the given ID!");
            string oldName = driver.Name;
            if (name.Equals(oldName) || name == "")
            {
                if (dateJoined != null)
                {
                    driver.EditDriver(name, dateJoined);
                    return Ok($"Driver {name} has had their join date set to {dateJoined.ToShortDateString()}");
                }
                else return BadRequest("No changes have been made.");
            }
            else
            {
                if (dateJoined != null)
                {
                    driver.EditDriver(name, dateJoined);
                    return Ok($"Driver {oldName} has been renamed to {name}, and has had their join date set to {dateJoined.ToShortDateString()}");
                }
                else
                {

                    driver.EditDriver(name, driver.DateJoined);
                    return Ok($"Driver {oldName} has been renamed to {name}");
                }
            }
        }



        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("api/Driver/delete/{id}")]
        public IHttpActionResult DeleteDriver(int id)
        {
            driver.GetDriver(id);
            if (driver != null)
            {
                driver.DeleteDriver();
                return Ok($"Driver {driver.Name} with ID {id} has been deleted.");
            }
            else
            {
                return Ok($"Driver with ID {id}is not in this system.");
            }
            //delete from archive if not in active list ********************
        }
    }
}