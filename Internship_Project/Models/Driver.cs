﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace Internship_Project.Models
{
    public class Driver
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsOnDuty { get; set; }
        public bool Active { get; set; }
        public DateTime DateJoined { get; set; }
        public DateTime LastActive { get; set; }
        public string CurrentOrder { get; set; }
        public int RideCost { get; set; }
        private SqlConnection conn = new SqlConnection(@"Data Source = ROBINLAPTOPN;Initial Catalog = Drivers;Trusted_Connection=True;");

        public static Driver myDriver;


        public void InsertNewDriver(string name, DateTime dateJoined)
        {
            myDriver = new Driver()
            {
                Name = name,
                DateJoined = dateJoined,
                LastActive = dateJoined
            };
            //update DriverState table

            DBquery($@"INSERT INTO DriverDetails (Name, DateJoined, LastActive) VALUES ('{myDriver.Name}', '{myDriver.DateJoined.ToShortDateString()}', '{myDriver.LastActive.ToShortDateString()}')");
        }



        public bool GetDriver(int id)
        {
            myDriver = new Driver()
            {
                Id = id
            };
            conn.Open();

            SqlCommand cmd = new SqlCommand($@"SELECT * FROM DriverDetails WHERE ID = {myDriver.Id}", conn);
            SqlDataReader read = cmd.ExecuteReader();

            if (read.Read())
            {
                Name = read["Name"].ToString();
                myDriver.DateJoined = Convert.ToDateTime(read["DateJoined"].ToString());
                myDriver.LastActive = Convert.ToDateTime(read["LastActive"].ToString());

                cmd = new SqlCommand($"SELECT * FROM DriverState WHERE ID = {myDriver.Id}", conn);
                read = cmd.ExecuteReader();
                while(read.Read())
                {
                    IsOnDuty = Convert.ToBoolean(read["State"].ToString());
                }

                conn.Close();
                return true;
            }
            else
            {
                myDriver = null;
                conn.Close();
                return false;
            }
        }



        public void EditDriver(string name, DateTime dateJoined)
        {
            myDriver.Name = name;
            myDriver.DateJoined = dateJoined;

            DBquery($@"UPDATE DriverDetails SET (Name = '{myDriver.Name}', DateJoined = '{myDriver.DateJoined.ToShortDateString()}')");
        }



        public void InsertDriverStatus(string currentOrder, int rideCost)
        {
            myDriver.CurrentOrder = currentOrder;
            myDriver.RideCost = rideCost;
            DateTime now = DateTime.Now;

            DBquery($@"INSERT INTO DriverStatus (ID, Date, IsOnDuty, CurrentOrder, RideCost) VALUES ({Id}, {now}, {IsOnDuty}, '{CurrentOrder}', {RideCost})");
        }



        public void UpdateStatus(bool active, bool duty)
        {
            myDriver.Active = active;
            myDriver.IsOnDuty = duty;
            DateTime now = DateTime.Now;
            int state = (active ? (duty ? 1 : 2) : 3);

            DBquery($@"INSERT INTO DriverState (ID, Date, State) VALUES ({Id}, {now}, {state})");
        }



        public static string CalculatePayment()
        {
            int days = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);

            //calculate seperately from times active and time on standby

            if (myDriver.IsOnDuty)
            {
                return "The amount to be paid is: " + (days * 100);
            }
            else
            {
                return "The amount to be paid is: " + (days * 70);
            }
        }



        public void DeleteDriver()
        {
            DBquery($@"DELETE * FROM DriverDetails WHERE ID = {myDriver.Id}");
        }



        //This method is to do any query in the database that does not read or is otherwise used
        public void DBquery(string query)
        {
            conn.Open();
            SqlCommand command = new SqlCommand(query, conn);
            command.ExecuteNonQuery();

            conn.Close();
        }



        public void ArchieveDataBaseRecords()
        {
            DateTime date = DateTime.Now.AddDays(-180);

            if (myDriver.LastActive < date)
            {
                conn.Open();

                //SQL Archive query untested
                SqlCommand cmd = new SqlCommand($@"(INSERT INTO INTO DriversArchive SELECT * FROM DriverDetails D WHERE D.LastActive < '{date.ToString()}') (DELETE * FROM DriversDetailTable WHERE  LastActive < '{date.ToString()}')", conn);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}