﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace Internship_Project.Models
{
    public class Admin
    {
        private string UserName { get; set; }
        private SqlConnection conn = new SqlConnection(@"Data Source = ROBINLAPTOPN;Initial Catalog = Drivers;Trusted_Connection=True;");

        public Admin currentAdmin;


        public void CreateAdmin(string userName, string password)
        {
            DBquery($@"INSERT INTO AdminDetails (UserName, Password) VALUES ('{userName}', '{password}')");
        }

        public void RemoveAdmin(string userName)
        {
            DBquery($"DELETE * FROM AdminDetails WHERE Username = '{userName}'");
        }

        public bool CheckAdmin(string userName, string password)
        {
            //check the admin details to see whether user is admin
            conn.Open();
            SqlCommand command = new SqlCommand("SELECT * FROM AdminDetails WHERE UserName = " + userName, conn);
            SqlDataReader read = command.ExecuteReader();
            if (read.Read())
            {
                if (read["Password"].ToString().Equals(password))
                {
                    if (currentAdmin == null)
                    {
                        currentAdmin = new Admin()
                        {
                            UserName = userName
                        };
                    }
                    conn.Close();
                    return true;
                }
                else
                {
                    conn.Close();
                    return false;
                }
            }
            else
            {
                conn.Close();
                return false;
            }
        }

        public bool CheckEmail(string userName, string email)
        {
            //check the admin details to see whether user is admin
            conn.Open();
            SqlCommand command = new SqlCommand("SELECT * FROM AdminDetails WHERE UserName = " + userName, conn);
            SqlDataReader read = command.ExecuteReader();
            if (read.Read())
            {
                if (read["Email"].ToString().Equals(email))
                {
                    conn.Close();
                    return true;
                }
                else
                {
                    conn.Close();
                    return false;
                }
            }
            else
            {
                conn.Close();
                return false;
            }
        }

        public void ChangeEmail(string userName, string email)
        {
            DBquery($"UPDATE AdminDetails SET (Email = '{email}') WHERE UserName = '{userName}'");
        }

        public bool ChangePass(string userName, string password)
        {
            conn.Open();
            SqlCommand command = new SqlCommand($"SELECT * FROM AdminDetails WHERE UserName = '{userName}'", conn);
            SqlDataReader read = command.ExecuteReader();
            if (read.Read())
            {
                if (password != "")
                {
                    command = new SqlCommand($"UPDATE AdminDetails SET (Password = '{password}')", conn);
                    command.ExecuteNonQuery();

                    conn.Close();
                    return true;
                }
                else
                {
                    conn.Close();
                    return false;
                }
            }
            else
            {
                conn.Close();
                return false;
            }
        }

        public void Logout()
        {
            currentAdmin = null;
        }

        //This method is to do any query in the database that does not read or is otherwise used
        public void DBquery(string query)
        {
            conn.Open();
            SqlCommand command = new SqlCommand(query, conn);
            command.ExecuteNonQuery();

            conn.Close();
        }
    }
}