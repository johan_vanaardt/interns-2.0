﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Internship_Project
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "AddAPI",
                routeTemplate: "api/{controller}/add/{id}/{dutyState}",
                defaults: new { id = RouteParameter.Optional }
                );

            config.Routes.MapHttpRoute(
                name: "EditAPI",
                routeTemplate: "api/{Controller}/edit/{id}/{name}/{onDutyStatus}",
                defaults: new { name = RouteParameter.Optional}
                );

            config.Routes.MapHttpRoute(
                name: "deleteAPI",
                routeTemplate: "api/Driver/delete/{id}"
                );
        }
    }
}
